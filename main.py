import kafka
import json

import paho.mqtt.client as mqtt

from settings import *

kafka_consumer = kafka.KafkaConsumer(KAFKA_TOPIC,
                               client_id=KAFKA_CLIENT,
                               bootstrap_servers=KAFKA_BOOTSTRAP_SERVERS,
                               security_protocol='SASL_PLAINTEXT',
                               sasl_mechanism='PLAIN',
                               sasl_plain_username=KAFKA_USERNAME,
                               sasl_plain_password=KAFKA_PASSWORD,
                               api_version=(0, 10, 1),
                               value_deserializer=lambda m: json.loads(m.decode('utf-8')))

mqtt_client = mqtt.Client()
mqtt_client.username_pw_set(username=MQTT_USERNAME, password=MQTT_PASSWORD)
mqtt_client.connect(MQTT_SERVER, port=int(MQTT_PORT))

for message in kafka_consumer:
    # qos=0 means that messages are received by the server at least once
    # retain=True means that the message will be set as the “last known good”/retained message for the topic
    mqtt_client.publish(MQTT_TOPIC, payload=str(message.value), qos=0, retain=True)
