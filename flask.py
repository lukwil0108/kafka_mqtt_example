# -*- coding: utf-8 -*-

import _thread

import kafka
import json

from settings import *

from flask import Flask, jsonify

app = Flask(__name__)
actual_state = {}

kafka_consumer = kafka.KafkaConsumer(KAFKA_TOPIC,
                               client_id=KAFKA_CLIENT,
                               bootstrap_servers=KAFKA_BOOTSTRAP_SERVERS,
                               security_protocol='SASL_PLAINTEXT',
                               sasl_mechanism='PLAIN',
                               sasl_plain_username=KAFKA_USERNAME,
                               sasl_plain_password=KAFKA_PASSWORD,
                               api_version=(0, 10, 1),
                               value_deserializer=lambda m: json.loads(m.decode('utf-8')))

@app.route('/')
def get_actual_state():
    return jsonify(actual_state)


def background_job():
    for message in kafka_consumer:
        global actual_state
        actual_state = message.value

_thread.start_new_thread(background_job, ())
