# Readme

## Command to generate MQTT accounts for connection (username/password)

```bash
docker run -it --rm -v $PWD/mosquitto:/mosquitto/config eclipse-mosquitto:1.6.8 mosquitto_passwd /mosquitto/config/mosquitto.passwd <username to generate password for>
```

## Build docker containers
```bash
docker-compose build
```

## Run containers in background
```bash
docker-compose up -d
```

